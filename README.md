# VFX Environment Setup

Welcome to the VFX Environment Setup repository! This project aims to streamline the setup process for VFX companies by providing scripts that configure the environment and launch Houdini and other applications with predefined settings.

## Features

- **Automated Environment Setup**: Uses shell scripts to configure the necessary environment variables and paths.
- **Convenient Launching**: Launch Houdini and other VFX applications with a single command, ensuring the environment is correctly set up each time.
- **Web-Based Interface**: Utilize a web browser interface to start applications with predefined environments.

## Installation

### Prerequisites

- [Node.js](https://nodejs.org/)
- [Houdini](https://www.sidefx.com/)

### Steps

1. **Clone the Repository**:
    ```sh
    git clone https://gitlab.com/Deviner/vfx-env-setup.git
    cd vfx-env-setup
    ```

2. **Install Node.js Dependencies**:

    npm install
    ```

3. **Update Shell Scripts**:
    - Open the shell scripts in the `scripts` directory.
    - Modify the paths to point to your local installations of Houdini and other applications.

## Usage

After completing the installation steps, you can start the web-based interface and launch the applications with predefined environments.

1. **Start the Web Interface**:
    ```sh
    npm start
    ```

2. **Access the Interface**:
    - Open your web browser and navigate to `http://localhost:3000`.

3. **Launch Applications**:
    - Select the desired application from the dropdown menu.
    - Click the "Launch" button to start the application with the predefined environment.

## Roadmap

- **Add More Tools and Applications**: Continuously expand the range of supported tools and applications.
- **Enhance Versatility**: Improve the flexibility of the setup scripts to support a wider range of environments and use cases.

## Contributing

We welcome contributions from the community! To contribute:

1. Fork the repository.
2. Create a new branch for your feature or bugfix.
    ```sh
    git checkout -b feature-name
    ```
3. Commit your changes.
    ```sh
    git commit -m "Description of changes"
    ```
4. Push to the branch.
    ```sh
    git push origin feature-name
    ```
5. Create a Merge Request on GitLab.

## License

This project is licensed under a proprietary license. You are free to use a similar approach for your own projects, but this repository and its contents are not freely distributable.

---

Thank you for using the VFX Environment Setup! If you have any questions or need further assistance, please feel free to open an issue or reach out to the maintainers.

![Description](misc/readme_img_v001.png)

