const express = require('express');
const bodyParser = require('body-parser');
const { exec } = require('child_process');
const path = require('path');
const fs = require('fs');
const os = require('os');

const app = express();
const port = 3000;
const setupFilePath = path.join(os.homedir(), '.project_launcher_setup.json');

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname))); // Serve static files from the root directory

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'start_page.html'));
});

app.get('/get-setup', (req, res) => {
    if (fs.existsSync(setupFilePath)) {
        const setupData = fs.readFileSync(setupFilePath);
        res.json(JSON.parse(setupData));
    } else {
        res.json({ project: '', username: '' });
    }
});

app.post('/launch-app', (req, res) => {
    const project = req.body.project;
    const username = req.body.username;
    const app = req.body.app;

    // Write the setup file
    const setupData = { project, username };
    fs.writeFileSync(setupFilePath, JSON.stringify(setupData));

    const script = './launch_script.sh';
    const args = `${project} ${app} ${username}`;

    exec(`${script} ${args}`, (error, stdout, stderr) => {
        if (error) {
            console.error(`exec error: ${error}`);
            res.status(500).json({ success: false, message: 'Script execution failed' });
            return;
        }

        console.log(`stdout: ${stdout}`);
        console.error(`stderr: ${stderr}`);
        res.json({ success: true });
    });
});

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});
