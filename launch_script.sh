#!/bin/bash
PROJECT_NAME=$1
APP_NAME=$2

echo "Launching $APP_NAME for project $PROJECT_NAME"

# Add your environment setup and app launch commands here
case $APP_NAME in
    Houdini)
        echo "Setting up environment for Houdini"
        houdini_launch.sh
        ;;
    *)
        echo "Unknown application: $APP_NAME"
        exit 1
        ;;
esac

